var Config;
Config = (function() {
    function Config(container) {
        var span;
        this.container = container;
        this.num_rows = 8;
        this.num_cols = 16;
        span = document.createElement("span");
        span.style.visibility = "hidden";
        this.container.append(span);
        span.innerHTML = "X";
        this.char_height = $(span).height();
        this.char_width = $(span).width();
        this.tile_height = this.char_height * this.num_rows;
        this.tile_width = this.char_width * this.num_cols;
        $(span).remove();
        this.default_content = Array(this.num_rows * this.num_cols + 1).join(" ");
    }
    Config.prototype.numRows = function() {
        return this.num_rows;
    };
    Config.prototype.numCols = function() {
        return this.num_cols;
    };
    Config.prototype.charHeight = function() {
        return this.char_height;
    };
    Config.prototype.charWidth = function() {
        return this.char_width;
    };
    Config.prototype.tileHeight = function() {
        return this.tile_height;
    };
    Config.prototype.tileWidth = function() {
        return this.tile_width;
    };
    Config.prototype.defaultContent = function() {
        return this.default_content;
    };
    return Config;
})();