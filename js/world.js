var World, __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    },
    __hasProp = {}.hasOwnProperty;
window.nextObjId = 1;
window.outputArray = [];
World = (function() {
    var makeBottomRoom;

    function World(_container, state) {
        var clickContainer, input, touchDevice, typeFirstChar, updateLastEvent, wheelScroll, _this = this;
        this._container = _container;
        this.typeChar = __bind(this.typeChar, this);
        this.setSelected = __bind(this.setSelected, this);
        this.scrollUpBy = __bind(this.scrollUpBy, this);
        this.scrollLeftBy = __bind(this.scrollLeftBy, this);
        this._scrollDirBy = __bind(this._scrollDirBy, this);
        this.coordLink = __bind(this.coordLink, this);
        this.sendUrlLink = __bind(this.sendUrlLink, this);
        this.sendCoordLink = __bind(this.sendCoordLink, this);
        this.goToCoord = __bind(this.goToCoord, this);
        this.urlLink = __bind(this.urlLink, this);
        this.getCoordInput = __bind(this.getCoordInput, this);
        this.doGoToCoord = __bind(this.doGoToCoord, this);
        this.doUrlLink = __bind(this.doUrlLink, this);
        this.doCoordLink = __bind(this.doCoordLink, this);
        this.unprotectATile = __bind(this.unprotectATile, this);
        this.protectATile = __bind(this.protectATile, this);
        this._cellActionUI = __bind(this._cellActionUI, this);
        this._actionUI = __bind(this._actionUI, this);
        this.doUnprotect = __bind(this.doUnprotect, this);
        this.doProtect = __bind(this.doProtect, this);
        this._tileAction = __bind(this._tileAction, this);
        this.cleanUpTiles = __bind(this.cleanUpTiles, this);
        this.moveCursor = __bind(this.moveCursor, this);
        this.fetchUpdates = __bind(this.fetchUpdates, this);
        this.sendEdit = __bind(this.sendEdit, this);
        this.updateData = __bind(this.updateData, this);
        this.makeRightRoom = __bind(this.makeRightRoom, this);
        this.makeTopRoom = __bind(this.makeTopRoom, this);
        this.makeLeftRoom = __bind(this.makeLeftRoom, this);
        this.renderMandatoryTiles = __bind(this.renderMandatoryTiles, this);
        this.getMandatoryTiles = __bind(this.getMandatoryTiles, this);
        this.getCenterCoords = __bind(this.getCenterCoords, this);
        this.makeRectangle = __bind(this.makeRectangle, this);
        this.setCoords = __bind(this.setCoords, this);
        this.getMandatoryBounds = __bind(this.getMandatoryBounds, this);
        this.getOrCreateTile = __bind(this.getOrCreateTile, this);
        this.createTile = __bind(this.createTile, this);
        this.getCell = __bind(this.getCell, this);
        this.getTile = __bind(this.getTile, this);
        this.rememberTile = __bind(this.rememberTile, this);
        this._buildMenu = __bind(this._buildMenu, this);
        this.setupWS = __bind(this.setupWS, this);
        $("#loading").hide();
        this.socket = null;
        this.wsQueue = [];
        this.socketChannel = null;
        this.setupWS();
        this._state = {
            selected: null,
            selectedCoords: null,
            offsetY: null,
            offsetX: null,
            numTiles: null,
            lastClick: null,
            lastEvent: new Date().getTime(),
            lastRender: null,
            announce: null,
            uiModal: false,
            goToCoord: {},
            initRendered: false,
            lastBounds: null,
            cursors: {}
        };
        $.extend(this._state, state);
        this.worldModel = state.worldModel;
        this.userModel = state.userModel;
        this._tileByCoord = {};
        this._inflightEdits = {};
        this._ui = {
            paused: $("#paused"),
            announce: $("#announce")
        };
        if (this._state.announce) {
            this._ui.announce.html(this._state.announce);
            this._ui.announce.show();
        }
        this._container.css({
            position: "relative",
            fontFamily: "Courier New",
            overflow: "hidden",
            background: "#ddd"
        });
        this._container.addClass("world-container");
        this._container.addClass("writability-" + Permissions.get_perm_display(this.worldModel.writability));
        this._container.height($(window).height());
        this._container.width($(window).width());
        this._config = new Config(this._container);
        Helpers.addCss("#yourworld table {height:" + this._config.tileHeight() + "px }");
        Helpers.addCss("#yourworld table {width:" + this._config.tileWidth() + "px }");
        Helpers.addCss(".tilecont {height:" + this._config.tileHeight() + "px }");
        Helpers.addCss(".tilecont {width:" + this._config.tileWidth() + "px }");
        this._state.offsetX = parseInt(this._container.width() / 2, 10);
        this._state.offsetY = parseInt(this._container.height() / 2, 10);
        this._state.numTiles = 0;
        this.renderMandatoryTiles();
        $(window).resize(function() {
            _this._container.height($(window).height());
            _this._container.width($(window).width());
            return _this.renderMandatoryTiles();
        });
        try {
            document.createEvent('TouchEvent');
            touchDevice = true;
        } catch (e) {
            touchDevice = false;
        }
        input = $('<textarea autocapitalize="off" autocorrect="off" autocomplete="off"></textarea>').css({
            position: 'absolute',
            left: '-1000px',
            top: '-1000px'
        }).appendTo($('body'))[0];
        if (touchDevice) {
            $(document).on('touchstart', function() {
                return input.focus();
            });
        }
        typeFirstChar = function() {
            var c, charAt;
            if (input.value) {
                charAt = Helpers.charAt(input.value, 0);
                if (charAt === "\n") {
                    c = _this._state.lastClick;
                    if (c && c.nodeName !== 'TD') {
                        c = null;
                    }
                    if (c) {
                        _this._state.lastClick = _this.moveCursor("down", c);
                    }
                } else {
                    _this.typeChar(charAt);
                    _this.moveCursor("right");
                }
                input.value = input.value.slice(Helpers.length(charAt));
                return true;
            }
            return false;
        };
        setInterval((function() {
            typeFirstChar();
            if (!Permissions.can_paste(_this.userModel, _this.worldModel)) {
                return input.value = "";
            }
        }), 10);
        $(document).keydown(function(e) {
            if (_this._state.uiModal) {
                return;
            }
            input.focus();
            if (e.keyCode === $.ui.keyCode.BACKSPACE) {
                _this.moveCursor("left");
                _this.typeChar(" ");
            } else if (e.keyCode === $.ui.keyCode.LEFT) {
                _this._state.lastClick = _this.moveCursor("left");
            } else if (e.keyCode === $.ui.keyCode.RIGHT) {
                _this._state.lastClick = _this.moveCursor("right");
            } else if (e.keyCode === $.ui.keyCode.DOWN) {
                _this._state.lastClick = _this.moveCursor("down");
            } else if (e.keyCode === $.ui.keyCode.UP) {
                _this._state.lastClick = _this.moveCursor("up");
            } else if (e.keyCode === $.ui.keyCode.ESCAPE) {
                $(_this._container).trigger("click");
            }
            input.value = "";
            return _this._state.lastEvent = new Date().getTime();
        });
        $(input).on("paste", function() {
            return _this._state.lastClick = _this._state.selected;
        });
        updateLastEvent = function(e) {
            return _this._state.lastEvent = new Date().getTime();
        };
        this._container.mousemove(updateLastEvent).on('touchmove', updateLastEvent);
        clickContainer = function(ev) {
            _this.setSelected(ev.target);
            _this._state.lastClick = ev.target;
            return input.value = "";
        };
        this._container.click(clickContainer).on('touchend', clickContainer);
        document.onselectstart = function() {
            return _this._state.uiModal;
        };
        this._container.css("-khtml-user-select", "none");
        this._container.css("-moz-user-select", "-moz-none");
        this._ui.scrolling = makeScrollable(this._container, function(dx, dy) {
            _this.scrollLeftBy(dx);
            return _this.scrollUpBy(dy);
        });
        setInterval(this.renderMandatoryTiles, 197);
        $(".coordLink").live("click", function(e) {
            var el;
            el = e.target;
            return _this.doGoToCoord($.data(el, "link_tileY"), $.data(el, "link_tileX"));
        });
        this.setCoords();
        this._buildMenu();
        $(document).bind("simplemodal_onopen", function() {
            return _this._state.uiModal = true;
        });
        $(document).bind("simplemodal_onclose", function() {
            return _this._state.uiModal = false;
        });
        wheelScroll = function(e) {
            var axis, h, i;
            if (_this._wheelTimeout) {
                return;
            }
            _this._wheelTimeout = setTimeout((function() {
                return _this._wheelTimeout = null;
            }), 100);
            h = e.originalEvent;
            i = 0;
            if (h.wheelDelta) {
                i = h.wheelDelta;
                if (window.opera) {
                    i = -1 * i;
                }
            } else if (h.detail) {
                i = -h.detail;
            }
            if (i) {
                i = 100 * (i > 0 ? 1 : -1);
                axis = h.axis === h.VERTICAL_AXIS ? 'y' : 'x';
                if (h.wheelDeltaX || h.wheelDeltaY) {
                    axis = h.wheelDeltaX ? 'x' : 'y';
                }
                if (axis === 'x') {
                    _this.scrollLeftBy(-i);
                } else {
                    _this.scrollUpBy(-i);
                }
                return _this.checkBoundsAndFetch();
            }
        };
        $('#yourworld').bind("mousewheel", wheelScroll).bind("DOMMouseScroll", wheelScroll);
    }
    World.prototype.setupWS = function() {
        var path, supportsWebSockets, ws_path, ws_scheme, _this = this;
        supportsWebSockets = typeof window.WebSocket === 'function';
        if (!supportsWebSockets) {
            $('#announce').html('Sorry, your browser is not supported').show();
            return;
        }
        ws_scheme = window.location.protocol === 'https:' ? 'wss' : 'ws';
        path = window.location.pathname.replace(/\/$/, '');
        ws_path = "ws://www.yourworldoftext.com" + window.location.search.replace(/\?/, '/') + "/ws/";
        this.socket = new ReconnectingWebSocket(ws_path);
        this.socket.onmessage = function(message) {
            var data, kind, methodName;
            data = JSON.parse(message.data);
            kind = data.kind.replace(/[a-z]/, function(str) {
                return str.toUpperCase();
            });
            methodName = "ws" + kind;
            return _this[methodName](data);
        };
        this.socket.onready = function(data) {
            var _i, _len, _ref;
            _this.socketChannel = data.channelName;
            _ref = _this.wsQueue;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                data = _ref[_i];
                _this.socket.send(data);
            }
            return _this.wsQueue = [];
        };
        this.socket.onopen = function() {
            return console.log('Connected to socket');
        };
        return this.socket.onclose = function() {
            $('.active-guest-cursor').removeClass('active-guest-cursor');
            return console.log('Disconnected form socket');
        };
    };
    World.prototype.wsSend = function(data) {
        data = JSON.stringify(data);
        if (this.socket.readyState === 1) {
            try {
                return this.socket.send(data);
            } catch (e) {
                return this.wsQueue.push(data);
            }
        } else {
            return this.wsQueue.push(data);
        }
    };
    World.prototype.wsIdentify = function(data) {
        this.socket.onready(data);
        return this.fetchUpdates();
    };
    World.prototype.wsTileUpdate = function(data) {
        if (data.sender && data.sender === this.socketChannel && data.source === 'write') {
            return;
        }
        return this.updateData(data.tiles);
    };
    World.prototype.highlightGuestCursor = function(position, highlight) {
        var $cell, charX, charY, tileX, tileY, _ref;
        position = position.join(",");
        _ref = $.map(position.split(","), function(pos) {
            return parseInt(pos);
        }), tileY = _ref[0], tileX = _ref[1], charY = _ref[2], charX = _ref[3];
        try {
            $cell = $(this.getCell(tileY, tileX, charY, charX));
            return $cell.toggleClass('active-guest-cursor', highlight);
        } catch (e) {}
    };
    World.prototype.wsWrite = function(data) {
        var editData, editId, reason, tile, _i, _len, _ref, _ref1, _results;
        _ref = data.accepted;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            editId = _ref[_i];
            editData = this._inflightEdits[editId];
            delete this._inflightEdits[editId];
            //tile = this.getTile(editData[0], editData[1]);
            //tile.editDone(editData[2], editData[3], editData[4], editData[5]);
        }
        _ref1 = data.rejected;
        _results = [];
        for (editId in _ref1) {
            reason = _ref1[editId];
            _results.push(delete this._inflightEdits[editId]);
        }
        return _results;
    };
    World.prototype.wsFetch = function(data) {
        window.latestFull = data.tiles
        return this.updateData(data.tiles);
    };
    World.prototype.checkBoundsAndFetch = function(data) {
        var bounds, lastBounds;
        bounds = this.getMandatoryBounds();
        lastBounds = this._state.lastBounds || "";
        if (bounds.toString() !== lastBounds.toString()) {
            this._state.lastBounds = bounds;
            return this.fetchUpdates();
        }
    };
    World.prototype.wsCursor = function(data) {
        var _base, _base1, _name, _name1;
        if (data.sender === this.socketChannel) {
            return;
        }
        if (data.current) {
            (_base = this._state.cursors)[_name = data.current] || (_base[_name] = 0);
            this._state.cursors[data.current] += 1;
            this.highlightGuestCursor(data.current, this._state.cursors[data.current] > 0);
        }
        if (data.previous) {
            (_base1 = this._state.cursors)[_name1 = data.previous] || (_base1[_name1] = 1);
            this._state.cursors[data.previous] -= 1;
            return this.highlightGuestCursor(data.previous, this._state.cursors[data.previous] > 0);
        }
    };
    World.prototype.wsColors = function() {
        var $link, href;
        $link = $('link#world-style');
        href = $link.attr('data-href');
        if (!href) {
            href = $link.attr('href');
            $link.attr('data-href', href);
        }
        return $link.attr('href', "" + href + "&t=" + (new Date().getTime()));
    };
    World.prototype.wsSendCursorPosition = function(previous, current) {
        var data;
        if (previous) {
            previous = Helpers.getCellCoords(previous);
        }
        if (current) {
            current = Helpers.getCellCoords(current);
        }
        data = {
            kind: "cursor",
            previous: previous,
            current: current
        };
        return this.wsSend(data);
    };
    World.prototype._buildMenu = function() {
        var menu, _this = this;
        menu = new Menu($("#menu"), $("#nav"));
        menu.addCheckboxOption(" Show coordinates", function() {
            return $("#coords").show();
        }, function() {
            return $("#coords").hide();
        });
        if (Permissions.can_go_to_coord(this.userModel, this.worldModel)) {
            menu.addOption("Go to coordinates", this.goToCoord);
        }
        if (Permissions.can_coordlink(this.userModel, this.worldModel)) {
            menu.addOption("Create link to coordinates", this.coordLink);
        }
        if (Permissions.can_urllink(this.userModel, this.worldModel)) {
            menu.addOption("Create link to URL", this.urlLink);
        }
        if (Permissions.can_admin(this.userModel, this.worldModel)) {
            menu.addOption("Make an area owner-only", function(event) {
                return _this.protectATile("owner-only");
            });
        }
        if (Permissions.can_protect_tiles(this.userModel, this.worldModel)) {
            menu.addOption("Make an area member-only", function(event) {
                return _this.protectATile("member-only");
            });
            menu.addOption("Make an area public", function(event) {
                return _this.protectATile("public");
            });
            return menu.addOption("Default area protection", this.unprotectATile);
        }
    };
    World.prototype.rememberTile = function(tileY, tileX, tileObj) {
        if (this._tileByCoord[tileY] === undefined) {
            this._tileByCoord[tileY] = {};
        }
        if (this._tileByCoord[tileY][tileX] !== undefined) {
            throw new Error("Recording same tile twice.");
        }
        return this._tileByCoord[tileY][tileX] = tileObj;
    };
    World.prototype.getTile = function(tileY, tileX) {
        if (!this._tileByCoord[tileY]) {
            return null;
        }
        return this._tileByCoord[tileY][tileX];
    };
    World.prototype.getCell = function(tileY, tileX, charY, charX) {
        return this.getTile(tileY, tileX).getCell(charY, charX);
    };
    World.prototype.createTile = function(tileY, tileX) {
        var tile, tileContainer;
        tileContainer = document.createElement("div");
        $.data(tileContainer, "tileY", tileY);
        $.data(tileContainer, "tileX", tileX);
        tileContainer.className = "tilecont";
        tileContainer.style.top = (this._config.tileHeight()) * tileY + this._state.offsetY + "px";
        tileContainer.style.left = (this._config.tileWidth()) * tileX + this._state.offsetX + "px";
        tile = new Tile(tileY, tileX, this._config, tileContainer);
        this.rememberTile(tileY, tileX, tile);
        this._container[0].appendChild(tileContainer);
        this._state.numTiles++;
        if ((this._state.numTiles % 1000) === 0) {
            setTimeout(this.cleanUpTiles, 0);
        }
        return tile;
    };
    World.prototype.getOrCreateTile = function(tileY, tileX) {
        return this.getTile(tileY, tileX) || this.createTile(tileY, tileX);
    };
    World.prototype.getMandatoryBounds = function() {
        var maxX, maxY, minVisX, minVisY, minX, minY, numAcross, numDown;
        minVisY = Math.floor((this._container.scrollTop() - this._state.offsetY) / this._config.tileHeight());
        minVisX = Math.floor((this._container.scrollLeft() - this._state.offsetX) / this._config.tileWidth());
        numDown = Math.ceil(this._container.height() / this._config.tileHeight());
        numAcross = Math.ceil(this._container.width() / this._config.tileWidth());
        minY = minVisY - 1;
        minX = minVisX - 1;
        maxY = minVisY + numDown + 2;
        maxX = minVisX + numAcross + 2;
        return [minY, minX, maxY, maxX];
    };
    World.prototype.setCoords = function() {
        var centerX, centerY, minVisX, minVisY, numAcross, numDown;
        minVisY = (this._container.scrollTop() - this._state.offsetY) / this._config.tileHeight();
        minVisX = (this._container.scrollLeft() - this._state.offsetX) / this._config.tileWidth();
        numDown = this._container.height() / this._config.tileHeight();
        numAcross = this._container.width() / this._config.tileWidth();
        centerY = minVisY + numDown / 2;
        centerX = minVisX + numAcross / 2;
        centerY = -Math.floor(centerY / 4);
        centerX = Math.floor(centerX / 4);
        $("#coord_Y").text(centerY);
        return $("#coord_X").text(centerX);
    };
    World.prototype.makeRectangle = function(minY, minX, maxY, maxX) {
        var coords, x, y, _i, _j;
        coords = [];
        for (y = _i = minY; minY <= maxY ? _i <= maxY : _i >= maxY; y = minY <= maxY ? ++_i : --_i) {
            for (x = _j = minX; minX <= maxX ? _j <= maxX : _j >= maxX; x = minX <= maxX ? ++_j : --_j) {
                coords.push([y, x]);
            }
        }
        return coords;
    };
    World.prototype.getCenterCoords = function() {
        var centerX, centerY, minVisX, minVisY, numAcross, numDown;
        minVisY = (this._container.scrollTop() - this._state.offsetY) / this._config.tileHeight();
        minVisX = (this._container.scrollLeft() - this._state.offsetX) / this._config.tileWidth();
        numDown = this._container.height() / this._config.tileHeight();
        numAcross = this._container.width() / this._config.tileWidth();
        centerY = minVisY + numDown / 2;
        centerX = minVisX + numAcross / 2;
        return [centerY, centerX];
    };
    World.prototype.getMandatoryTiles = function() {
        return this.makeRectangle.apply(this, this.getMandatoryBounds());
    };
    World.prototype.renderMandatoryTiles = function() {
        var bounds, x, y, _i, _len, _ref, _ref1, _results;
        bounds = this.getMandatoryBounds();
        if (this._state.lastRender && Helpers.deepEquals(bounds, this._state.lastRender)) {
            return;
        }
        this._state.lastRender = bounds;
        _ref = this.makeRectangle.apply(this, bounds);
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            _ref1 = _ref[_i], y = _ref1[0], x = _ref1[1];
            _results.push(this.getOrCreateTile(y, x));
        }
        return _results;
    };
    World.prototype.makeLeftRoom = function(numPx) {
        var room;
        room = this._config.tileWidth() * 5;
        if (numPx > room) {
            throw new Error("no big jumps yet");
        }
        this._state.offsetX += room;
        $(".tilecont").each(function() {
            return this.style.left = parseInt(this.style.left, 10) + room + "px";
        });
        this._container.scrollLeft(this._container.scrollLeft() + room);
        return room;
    };
    World.prototype.makeTopRoom = function(numPx) {
        var room;
        room = this._config.tileHeight() * 5;
        if (numPx > room) {
            throw new Error("no big jumps yet");
        }
        this._state.offsetY += room;
        $(".tilecont").each(function() {
            return this.style.top = parseInt(this.style.top, 10) + room + "px";
        });
        this._container.scrollTop(this._container.scrollTop() + room);
        return room;
    };
    World.prototype.makeRightRoom = function(numPx) {
        var bounds, maxX, maxY;
        bounds = this.getMandatoryBounds();
        maxY = bounds[2];
        maxX = bounds[3];
        return this.getOrCreateTile(maxY + 5, maxX + 5);
    };
    makeBottomRoom = World.makeRightRoom;
    World.prototype.updateData = function(data) {
        var _this = this;
        return $.each(data, function(yx, properties) {
            var tile, x, y, _ref;
            _ref = yx.split(","), y = _ref[0], x = _ref[1];
            tile = _this.getTile(y, x);
            if (tile) {
                return tile.setProperties(properties);
            }
        });
    };
    World.prototype.sendEdit = function(editId, edit) {
        var data;
        data = {
            kind: "write",
            edits: [edit]
        };
        this._inflightEdits[editId] = edit;
        return this.wsSend(data);
    };
    World.prototype.fetchUpdates = function() {
        var bounds, data;
        if ((new Date().getTime() - this._state.lastEvent) > 30000) {
            this._ui.paused.show();
            setTimeout(this.fetchUpdates, 331);
            return;
        }
        this._ui.paused.hide();
        bounds = this.getMandatoryBounds();
        data = {
            kind: "fetch",
            min_tileY: bounds[0],
            min_tileX: bounds[1],
            max_tileY: bounds[2],
            max_tileX: bounds[3],
            v: 3
        };
        return this.wsSend(data);
    };
    World.prototype.moveCursor = function(dir, opt_from) {
        var charX, charY, from, target, tileX, tileY, _ref;
        from = opt_from || this._state.selected;
        if (!from) {
            return;
        }
        _ref = Helpers.getCellCoords(from), tileY = _ref[0], tileX = _ref[1], charY = _ref[2], charX = _ref[3];
        if (dir === "right") {
            if (charX === this._config.numCols() - 1) {
                assert(this.getOrCreateTile(tileY, tileX + 1));
                charX = 0;
                tileX++;
            } else {
                charX++;
            }
        } else if (dir === "left") {
            if (charX === 0) {
                assert(this.getOrCreateTile(tileY, tileX - 1));
                charX = this._config.numCols() - 1;
                tileX--;
            } else {
                charX--;
            }
        } else if (dir === "down") {
            if (charY === this._config.numRows() - 1) {
                assert(this.getOrCreateTile(tileY + 1, tileX));
                charY = 0;
                tileY++;
            } else {
                charY++;
            }
        } else if (dir === "up") {
            if (charY === 0) {
                assert(this.getOrCreateTile(tileY - 1, tileX));
                charY = this._config.numRows() - 1;
                tileY--;
            } else {
                charY--;
            }
        } else {
            throw new Error("Unknown direction to move.");
        }
        //editsArray = [tileY, tileX, charY, charX, "", "∗", ''];
        //window.outputArray.push(editsArray);
        target = this.getCell(tileY, tileX, charY, charX);
        this.setSelected(target);
        return target;
    };
    World.prototype.cleanUpTiles = function() {
        var mandatory, now, numRequired, rowObj, tile, tileX, tileY, _i, _len, _ref, _ref1, _results;
        now = new Date().getTime();
        mandatory = this.getMandatoryTiles();
        numRequired = mandatory.length;
        if (this._state.numTiles < 3 * numRequired) {
            return;
        }
        for (_i = 0, _len = mandatory.length; _i < _len; _i++) {
            _ref = mandatory[_i], tileY = _ref[0], tileX = _ref[1];
            tile = this.getTile(tileY, tileX);
            if (tile) {
                tile.neededAt = now;
            }
        }
        _ref1 = this._tileByCoord;
        _results = [];
        for (tileY in _ref1) {
            if (!__hasProp.call(_ref1, tileY)) continue;
            rowObj = _ref1[tileY];
            _results.push((function() {
                var _results1;
                _results1 = [];
                for (tileX in rowObj) {
                    if (!__hasProp.call(rowObj, tileX)) continue;
                    tile = rowObj[tileX];
                    if (tile.neededAt !== now) {
                        tile.remove();
                        delete this._tileByCoord[tileY][tileX];
                        _results1.push(this._state.numTiles--);
                    } else {
                        _results1.push(void 0);
                    }
                }
                return _results1;
            }).call(this));
        }
        return _results;
    };
    World.prototype._tileAction = function(url, tile, opt_args) {
        var data;
        data = {
            world: this.worldModel.path,
            tileY: $.data(tile, "tileY"),
            tileX: $.data(tile, "tileX")
        };
        if (opt_args) {
            $.extend(data, opt_args);
        }
        return jQuery.ajax({
            type: "POST",
            url: url,
            data: data
        });
    };
    World.prototype.doProtect = function(tile, type) {
        var args;
        args = {
            type: type || "owner-only"
        };
        return this._tileAction("/ajax/protect/", tile, args);
    };
    World.prototype.doUnprotect = function(tile) {
        return this._tileAction("/ajax/unprotect/", tile);
    };
    World.prototype._actionUI = function(styles, elType, callback, opt_extra_args) {
        var css, _this = this;
        this._ui.scrolling.stop();
        styles = [(function() {
            var _i, _len, _results;
            _results = [];
            for (_i = 0, _len = styles.length; _i < _len; _i++) {
                css = styles[_i];
                _results.push(Helpers.addCss(css));
            }
            return _results;
        })()];
        return $(this._container).one("click", function(e) {
            var args, stylesheet, target, _i, _len;
            target = $(e.target).closest(elType).get(0);
            if (target) {
                args = [target];
                args.push.apply(args, opt_extra_args || []);
                callback.apply(_this, args);
            }
            for (_i = 0, _len = styles.length; _i < _len; _i++) {
                stylesheet = styles[_i];
                $(stylesheet).remove();
            }
            return _this._ui.scrolling.start();
        });
    };
    World.prototype._tileActionUI = function(bgColor, callback, opt_arg) {
        var styles;
        log("using bgcolor", bgColor);
        styles = [".tilecont:hover {background-color: " + bgColor + " !important; cursor:pointer}"];
        return this._actionUI(styles, '.tilecont', callback, [opt_arg]);
    };
    World.prototype._cellActionUI = function(callback, args) {
        var styles;
        styles = ["td:hover {background-color: #aaf; cursor:pointer}"];
        if (!Permissions.can_admin(this.userModel, this.worldModel)) {
            styles.push(".protected td:hover {background-color: inherit; cursor:inherit}");
        }
        return this._actionUI(styles, 'td', callback, args);
    };
    World.prototype.protectATile = function(protectType) {
        var bgColor;
        bgColor = {
            'owner-only': '#ddd',
            'member-only': '#eee',
            'public': '#fff'
        }[protectType];
        return this._tileActionUI(bgColor, this.doProtect, protectType);
    };
    World.prototype.unprotectATile = function() {
        return this._tileActionUI('#fff', this.doUnprotect);
    };
    World.prototype.doCoordLink = function(y, x) {
        return this._cellActionUI(this.sendCoordLink, [y, x]);
    };
    World.prototype.doUrlLink = function(url) {
        return this._cellActionUI(this.sendUrlLink, [url]);
    };
    World.prototype.doGoToCoord = function(y, x) {
        var scroller, _this = this;
        y *= -4;
        x *= 4;
        y += 2;
        x += 2;
        if (!this._state.goToCoord.initted) {
            this._state.goToCoord.cancel = function() {
                clearInterval(_this._state.goToCoord.interval);
                _this._state.lastEvent = new Date().getTime();
                return $(document).trigger("YWOT_GoToCoord_stop");
            };
            $(document).bind("YWOT_GoToCoord_start", function() {
                return $(document).bind("mousedown", _this._state.goToCoord.cancel);
            });
            $(document).bind("YWOT_GoToCoord_stop", function() {
                $(document).unbind("mousedown", _this._state.goToCoord.cancel);
                return _this.checkBoundsAndFetch();
            });
            this._state.goToCoord.initted = true;
        }
        scroller = function() {
            var centerX, centerY, distance, xDiff, xMove, yDiff, yMove, _ref;
            _ref = _this.getCenterCoords(), centerY = _ref[0], centerX = _ref[1];
            yDiff = y - centerY;
            xDiff = x - centerX;
            yDiff *= _this._config.tileHeight();
            xDiff *= _this._config.tileWidth();
            distance = Helpers.vectorLen(yDiff, xDiff);
            yMove = Math.round(yDiff * 20 / distance);
            xMove = Math.round(xDiff * 20 / distance);
            if (Helpers.vectorLen(yDiff, xDiff) < 40) {
                _this._state.goToCoord.cancel();
                return;
            }
            yDiff = yDiff - yMove;
            _this.scrollUpBy(yMove);
            xDiff = xDiff - xMove;
            return _this.scrollLeftBy(xMove);
        };
        this._state.goToCoord.interval = setInterval(scroller, 25);
        return $(document).trigger("YWOT_GoToCoord_start");
    };
    World.prototype.getCoordInput = function(title, callback) {
        var d, html, _this = this;
        if (this._ui.coordInputModal) {
            d = this._ui.coordInputModal;
        } else {
            d = document.createElement("div");
            html = [];
            html.push("<form method=\"get\" action=\"#\" id=\"coord_input_form\">");
            html.push("<div id=\"coord_input_title\" style=\"max-width:20em\"></div><br>");
            html.push("<table>");
            html.push("<tr><td>X: </td><td><input type=\"text\" name=\"coord_input_X\" value=\"\"></td></tr>");
            html.push("<tr><td>Y: </td><td><input type=\"text\" name=\"coord_input_Y\" value=\"\"></td></tr>");
            html.push("</table>");
            html.push("<div id=\"coord_input_submit\"><input type=\"submit\" value=\"   Go   \"> or <span id=\"coord_input_cancel\" class=\"simplemodal-close simplemodal-closelink\">cancel</span></div>");
            html.push("</form>");
            d.innerHTML = html.join("");
            $(d).hide();
            $("body").append(d);
            this._ui.coordInputModal = d;
            $("#coord_input_form").submit(function() {
                var f, fail, x, y;
                f = document.getElementById("coord_input_form");
                y = parseInt(f.coord_input_Y.value, 10);
                x = parseInt(f.coord_input_X.value, 10);
                fail = false;
                if (isNaN(y)) {
                    fail = true;
                    $(f.coord_input_Y).css("border", "1px solid red");
                } else {
                    $(f.coord_input_Y).css("border", "");
                    f.coord_input_Y.value = y;
                }
                if (isNaN(x)) {
                    fail = true;
                    $(f.coord_input_X).css("border", "1px solid red");
                } else {
                    $(f.coord_input_X).css("border", "");
                    f.coord_input_X.value = x;
                }
                if (!fail) {
                    $("#coord_input_cancel").trigger("click");
                    setTimeout((function() {
                        return _this._ui.coordInputCallback(y, x);
                    }), 0);
                }
                return false;
            });
        }
        $("#coord_input_title").text(title);
        this._ui.coordInputCallback = callback;
        return $(d).modal({
            minHeight: 80,
            minWidth: 160,
            persist: true
        });
    };
    World.prototype.urlLink = function() {
        var d, html, _this = this;
        if (this._ui.urlInputModal) {
            d = this._ui.urlInputModal;
        } else {
            d = document.createElement("div");
            html = [];
            html.push("<form method=\"get\" action=\"#\" id=\"url_input_form\">");
            html.push("<div id=\"url_input_title\" style=\"max-width:20em\"></div><br>");
            html.push("<label for=\"url_input\">URL: </label><input type=\"text\" name=\"url_input\" value=\"\">");
            html.push("<div id=\"url_input_submit\"><input type=\"submit\" value=\"   Go   \"> or <span id=\"url_input_cancel\" class=\"simplemodal-close simplemodal-closelink\">cancel</span></div>");
            html.push("</form>");
            d.innerHTML = html.join("");
            $(d).hide();
            $("body").append(d);
            this._ui.urlInputModal = d;
            $("#url_input_form").submit(function() {
                var url;
                url = document.getElementById("url_input_form").url_input.value;
                $("#url_input_cancel").trigger("click");
                setTimeout((function() {
                    return _this.doUrlLink(url);
                }), 0);
                return false;
            });
        }
        return $(d).modal({
            minHeight: 80,
            minWidth: 160,
            persist: true
        });
    };
    World.prototype.goToCoord = function() {
        return this.getCoordInput("Go to coordinates:", this.doGoToCoord);
    };
    World.prototype._sendCellLink = function(td, url, extra_data) {
        var charX, charY, data, tileX, tileY, _ref;
        _ref = Helpers.getCellCoords(td), tileY = _ref[0], tileX = _ref[1], charY = _ref[2], charX = _ref[3];
        data = {
            world: this.worldModel.path,
            tileY: tileY,
            tileX: tileX,
            charY: charY,
            charX: charX
        };
        $.extend(data, extra_data);
        return jQuery.ajax({
            type: "POST",
            url: url,
            data: data
        });
    };
    World.prototype.sendCoordLink = function(td, y, x) {
        return this._sendCellLink(td, "/ajax/coordlink/", {
            link_tileY: y,
            link_tileX: x
        });
    };
    World.prototype.sendUrlLink = function(td, url) {
        return this._sendCellLink(td, "/ajax/urllink/", {
            url: url
        });
    };
    World.prototype.coordLink = function() {
        return this.getCoordInput("Enter the coordinates to create a link to. You can then click on a letter to create the link.", this.doCoordLink);
    };
    World.prototype._scrollDirBy = function(scrollAttr, makeNegativeRoom, makePositiveRoom, scrollSizeAttr, sizeAttr, distance) {
        var newPosition, offset, positiveRoom;
        newPosition = this._container[scrollAttr]() + distance;
        if (newPosition < 0) {
            offset = makeNegativeRoom(-newPosition);
            newPosition = newPosition + offset;
        } else {
            positiveRoom = this._container.attr(scrollSizeAttr) - newPosition - this._container[sizeAttr]();
            if (positiveRoom < 0) {
                makePositiveRoom(-positiveRoom);
            }
        }
        this._container[scrollAttr](newPosition);
        return this.setCoords();
    };
    World.prototype.scrollLeftBy = function(dx) {
        return this._scrollDirBy('scrollLeft', this.makeLeftRoom, this.makeRightRoom, 'scrollWidth', 'width', dx);
    };
    World.prototype.scrollUpBy = function(dy) {
        return this._scrollDirBy('scrollTop', this.makeTopRoom, this.makeBottomRoom, 'scrollHeight', 'height', dy);
    };
    World.prototype.setSelected = function(el) {
        var btmRoom, charX, charY, e, leftRoom, previous, rightRoom, tile, tileX, tileY, topRoom, _ref;
        if (this._state.selected) {
            $(this._state.selected).removeClass('active-cursor');
        }
        previous = this._state.selected;
        this._state.selected = null;
        if (!el || el.nodeName !== "TD") {
            return;
        }
        _ref = Helpers.getCellCoords(el), tileY = _ref[0], tileX = _ref[1], charY = _ref[2], charX = _ref[3];
        tile = this.getTile(tileY, tileX);
        if (!tile.initted()) {
            return;
        }
        if (!Permissions.can_edit_tile(this.userModel, this.worldModel, tile)) {
            return;
        }
        e = $(el);
        rightRoom = this._container.offset().left + this._container.width() - e.offset().left - e.width();
        if (rightRoom < 0) {
            this.scrollLeftBy(Math.ceil(-rightRoom / this._config.charWidth()) * this._config.charWidth());
        }
        btmRoom = this._container.offset().top + this._container.height() - e.offset().top - e.height();
        if (btmRoom < 0) {
            this.scrollUpBy(Math.ceil(-btmRoom / this._config.charHeight()) * this._config.charHeight());
        }
        leftRoom = e.offset().left - this._container.offset().left - e.width();
        if (leftRoom < 0) {
            this.scrollLeftBy(Math.ceil(leftRoom / this._config.charWidth()) * this._config.charWidth());
        }
        topRoom = e.offset().top - this._container.offset().top - e.height();
        if (topRoom < 0) {
            this.scrollUpBy(Math.ceil(topRoom / this._config.charHeight()) * this._config.charHeight());
        }
        e.addClass('active-cursor');
        this._state.selected = el;
        this._state.selectedCoords = "" + tileY + "," + tileX + "," + charY + "," + charX;
        this.wsSendCursorPosition(previous, el);
        return this.checkBoundsAndFetch();
    };
    World.prototype.typeChar = function(s) {
        var charX, charY, editData, editId, tile, tileX, tileY, timestamp, _ref;
        if (Helpers.length(s) !== 1) {
            throw new Error("I thought I was only getting one letter");
        }
        if (!this._state.selected) {
            return;
        }
        _ref = Helpers.getCellCoords(this._state.selected), tileY = _ref[0], tileX = _ref[1], charY = _ref[2], charX = _ref[3];
        tile = this.getTile(tileY, tileX);
        if (!tile) {
            throw new Error("tile to update not found");
        }
        if (!Permissions.can_edit_tile(this.userModel, this.worldModel, tile)) {
            return;
        }
        this._state.selected.innerHTML = Helpers.escapeChar(s);
        timestamp = new Date().getTime();
        tile.tellEdit(charY, charX, s, timestamp);
        editId = window.nextObjId++;
        editData = [tileY, tileX, charY, charX, timestamp, s, editId];
        return this.sendEdit(editId, editData);
    };
    return World;
})();

